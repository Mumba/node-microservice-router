# Mumba Microservice Router

## Description

Router support for Microservices.

## Installation 

```sh
$ npm install --save mumba-microservice-router
```

## Example

TODO

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm run docker:up
$ npm test
$ npm run docker:down
```

## People

The original author of _Mumba WAMP_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

