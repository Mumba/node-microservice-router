/**
 * Microservice Router
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Subject} from 'rxjs/Subject';

/**
 * Microservice router interface.
 */
export interface Router {
	onRegister: Subject<any>;
	onSubscribe: Subject<any>;
	onError: Subject<any>;

	/**
	 * Register a callable function with a name.
	 *
	 * Some routers may support both argument lists and parameter dictionaries. For the former, an optional transform
	 * can be used to standardise the callback (to use a parameter dictionary).
	 *
	 * @param {string}   name           - The name of the action/procedure/command.
	 * @param {Function} callback       - The function to call.
	 * @return this
	 */
	register(name: string, callback: Function): this;

	/**
	 * Subscribe to a topic registered on the router.
	 *
	 * @param {string}   topic            - the URI of the topic to subscribe to.
	 * @param {callable} handler          - the event handler that should consume events.
	 * @param {object}   [options]
	 * @param {string}   [options.match]  - `prefix`, `wildcard`. Default: nothing (exact matching).
	 * @return this
	 * @see {@link http://autobahn.ws/js/reference.html#subscribe}
	 */
	subscribe(topic: string, handler: Function, options?: any): this;
}
