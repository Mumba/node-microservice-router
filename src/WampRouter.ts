/**
 * Microservice Router
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Subject} from 'rxjs/Subject';
import {WampClient, WampSession} from 'mumba-wamp';
import {Router} from './interfaces/Router';

/**
 * WAMP microservice router.
 */
export class WampRouter implements Router {
	public onRegister: Subject<any>;
	public onSubscribe: Subject<any>;
	public onError: Subject<any>;

	private wampClient: WampClient;
	private sessionApi: WampSession;

	/**
	 * @param {WampClient}  wampClient     - The client to connect to the WAMP router.
	 * @param {WampSession} wampSessionApi - The session API for the WAMP router.
	 */
	constructor(wampClient: WampClient, wampSessionApi: WampSession) {
		if (!(wampClient instanceof WampClient)) {
			throw new Error('WampRouter: <wampClient> required.');
		}

		if (!(wampSessionApi instanceof WampSession)) {
			throw new Error('WampRouter: <wampSessionApi> required.');
		}

		this.wampClient = wampClient;
		this.sessionApi = wampSessionApi;

		this.onRegister = wampClient.onRegister;
		this.onSubscribe = wampClient.onSubscribe;
		this.onError = wampClient.onError;
	}

	/**
	 * Register a procedure on the WAMP router.
	 *
	 * @param {string}   name           - The name of the action.
	 * @param {Function} callback       - The function to call.
	 * @returns this
	 */
	register(name: string, callback: Function): this {
		this.wampClient.register(name, (args: any[], kwargs: {}, details: any) => {
			return this.sessionApi.get(details.caller)
				.then((caller: any) => {
					return callback(caller.authid, args, kwargs)
				});
		});

		return this;
	}

	/**
	 * Subscribe to a topic on a session.
	 *
	 * @param {string}   topic            - the URI of the topic to subscribe to.
	 * @param {callable} handler          - the event handler that should consume events.
	 * @param {object}   [options]
	 * @param {string}   [options.match]  - `prefix`, `wildcard`. Default: nothing (exact matching).
	 * @returns this
	 * @see {@link http://autobahn.ws/js/reference.html#subscribe}
	 */
	subscribe(topic: string, handler: Function, options?: any): this {
		this.wampClient.subscribe(topic, (args: any[], kwargs: any, details: any) => {
			return this.sessionApi.get(details.publisher)
				.then((caller: any) => {
					return handler(caller.authid, args, kwargs)
				});
		});

		return this;
	}
}
