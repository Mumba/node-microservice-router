/**
 * Microservice Router public exports.
 *
 * @copyright Mumba Pty Ltd 2016. All rights reserved.
 * @license   Apache-2.0
 */

export {Router} from './interfaces/Router';
export {WampRouter} from './WampRouter';
