# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased][unreleased]

## [0.2.1] 29 May 2017
- Updated deps (Autobahn upgrade).

## [0.2.0] 16 Nov 2016
- Updated deps.
- Added `Router.onError`.

## [0.1.0] 21 Jul 2016
- Initial release
