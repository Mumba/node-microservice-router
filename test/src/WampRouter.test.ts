/**
 * WampRouter tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {WampClient, WampSession, WampChallenge} from 'mumba-wamp';

import {Router, WampRouter} from '../../src/index';
import {createOptions} from '../bootstrap';

describe('WampRouter unit tests', () => {
	let wampClientSpy: WampClient;
	let sessionApiSpy: WampSession;
	let instance: Router;

	beforeEach(() => {
		wampClientSpy = Object.create(WampClient.prototype);
		sessionApiSpy = Object.create(WampSession.prototype);
		instance = new WampRouter(wampClientSpy, sessionApiSpy);
	});

	it('should throw if `wampClient` is missing in the constructor', () => {
		assert.throws(() => {
			new WampRouter(void 0, sessionApiSpy);
		}, /<wampClient> required/);
	});

	it('should throw if `wampSessionApi` is missing in the constructor', () => {
		assert.throws(() => {
			new WampRouter(wampClientSpy, void 0);
		}, /<wampSessionApi> required/);
	});
});

describe('WampRouter functional tests', () => {
	let wampClient: WampClient;
	let instance: WampRouter;

	beforeEach(function (done) {
		let challenge = new WampChallenge();
		let options = createOptions(challenge.createWampCra('*password'))

		wampClient = new WampClient(options);

		let sessionApi = new WampSession(wampClient);

		instance = new WampRouter(wampClient, sessionApi);

		wampClient.onOpen.subscribe(() => {
			done();
		});

		wampClient.openConnection();
	});

	afterEach(function () {
		wampClient.closeConnection();
	});

	it('should register an action with the WAMP router', (done) => {
		let actionSpy = (authid: number, args: any[], kwargs: any) => {
			assert.equal(authid, 1, 'should pass the correct authid');
			assert.deepEqual(args, [101], 'should pass the correct args');
			assert.deepEqual(kwargs, { foo: 'bar' }, 'should pass the correct kwargs');

			return 'judo';
		};

		// Call the end-point when it is registered.
		instance.onRegister.subscribe(() => {
			wampClient.call('kung.foo', [101], { foo: 'bar' })
				.then((response: any) => {
					assert.equal(response, 'judo', 'should get the response from the action');
					done();
				})
				.catch(done)
		});

		instance.register('kung.foo', actionSpy);
	});

	it('it should subscribe to a topic on the WAMP router', (done) => {
		instance.onSubscribe.subscribe((result: any) => {
			assert.equal(result.topic, 'about', 'should have subscribed');

			wampClient.publish('about', [101], { foo: 'bar' }, {
				exclude_me: false
			})
				.catch(done);
		});

		instance.subscribe('about', (authid: any, args: any[], kwargs: any) => {
			try {
				assert.equal(authid, 1, 'should pass the correct authid');
				assert.deepEqual(args, [101], 'should pass the correct args');
				assert.deepEqual(kwargs, { foo: 'bar' }, 'should pass the correct kwargs');
				done();
			}
			catch (e) {
				done(e);
			}
		});
	});

	it('should subscribe to error', (done) => {
		instance.onError.subscribe((err: Error) => {
			assert.equal(err.message, 'the-error', 'should be the error');
			done();
		});
		wampClient.onError.next(new Error('the-error'));
	});
});
