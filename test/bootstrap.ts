/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const HOST = process.env.HOST || 'localhost';

/**
 * Create options for the WAMP connection.
 *
 * @param {Function} [challenge]
 * @returns {*}
 */
export function createOptions(challenge: any): any {
	return {
		url: 'ws://' + HOST + ':8080',
		realm: 'test-realm',
		authid: '1',
		authmethods: ['wampcra'],
		onchallenge: challenge,
		max_retries: 0
	}
}
